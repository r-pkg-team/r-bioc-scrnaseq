Package: scRNAseq
Title: Collection of Public Single-Cell RNA-Seq Datasets
Version: 2.20.0
Date: 2024-06-20
Authors@R: c(
    person("Davide", "Risso", email = "risso.davide@gmail.com", role = c("aut", "cph")),
    person("Michael", "Cole", role="aut"),
    person("Aaron", "Lun", role=c("ctb", "cre"), email="infinite.monkeys.with.keyboards@gmail.com"),
    person("Alan", "O'Callaghan", role="ctb"),
    person("Jens", "Preussner", role="ctb"),
    person("Charlotte", "Soneson", role="ctb"),
    person("Stephany", "Orjuela", role="ctb"),
    person("Daniel", "Bunis", role="ctb"),
    person("Milan", "Malfait", role="ctb"))
Description: Gene-level counts for a collection of public scRNA-seq datasets,
    provided as SingleCellExperiment objects with cell- and gene-level metadata.
License: CC0
NeedsCompilation: no
Depends: SingleCellExperiment
Imports: utils, methods, Matrix, BiocGenerics, S4Vectors, SparseArray,
        DelayedArray, GenomicRanges, SummarizedExperiment,
        ExperimentHub (>= 2.3.4), AnnotationHub (>= 3.3.6),
        AnnotationDbi, ensembldb, GenomicFeatures, alabaster.base,
        alabaster.matrix, alabaster.sce, gypsum, jsonlite, DBI, RSQLite
Suggests: BiocStyle, knitr, rmarkdown, testthat, jsonvalidate,
        BiocManager
VignetteBuilder: knitr
Encoding: UTF-8
biocViews: ExperimentHub, ExperimentData, ExpressionData,
        SequencingData, RNASeqData, SingleCellData
BuildResaveData: no
RoxygenNote: 7.3.1
git_url: https://git.bioconductor.org/packages/scRNAseq
git_branch: RELEASE_3_20
git_last_commit: 40e3e5b
git_last_commit_date: 2024-10-29
Repository: Bioconductor 3.20
Date/Publication: 2024-10-31
Packaged: 2024-10-31 17:02:46 UTC; biocbuild
Author: Davide Risso [aut, cph],
  Michael Cole [aut],
  Aaron Lun [ctb, cre],
  Alan O'Callaghan [ctb],
  Jens Preussner [ctb],
  Charlotte Soneson [ctb],
  Stephany Orjuela [ctb],
  Daniel Bunis [ctb],
  Milan Malfait [ctb]
Maintainer: Aaron Lun <infinite.monkeys.with.keyboards@gmail.com>
